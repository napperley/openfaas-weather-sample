FROM debian:stretch-slim
ADD https://github.com/openfaas/faas/releases/download/0.19.1/fwatchdog /usr/bin
RUN chmod +x /usr/bin/fwatchdog
RUN mkdir -p /app
COPY openweathermap_key.txt /app
COPY build/bin/linuxX64/weatherReleaseExecutable/weather.kexe /app/weather
RUN chmod +x /app/weather
ENV fprocess "./weather"
HEALTHCHECK --interval=2s CMD [ -e /tmp/.lock ] || exit 1
CMD ["fwatchdog"]
