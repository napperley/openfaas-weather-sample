@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.weatherFunc

import curl.*
import kotlinx.cinterop.*
import kotlinx.serialization.json.Json
import org.example.weatherFunc.model.WeatherResult
import platform.posix.fprintf
import platform.posix.stderr
import kotlin.system.exitProcess

private val curlHandle by lazy {
    val result = curl_easy_init()
    // Set the HTTP read body handler.
    curl_easy_setopt(result, CURLOPT_WRITEFUNCTION, staticCFunction(::readHttpBody))
    result
}
private val json = Json { ignoreUnknownKeys = true }
private var httpBody = ""

private fun readHttpBody(
    buffer: CPointer<ByteVar>?,
    size: ULong,
    totalItems: ULong,
    @Suppress("UNUSED_PARAMETER") userdata: COpaquePointer?
): ULong {
    initRuntimeIfNeeded()
    httpBody = buffer?.toKString()?.trim() ?: ""
    return size * totalItems
}

internal actual suspend fun fetchWeather(location: String): WeatherResult {
    changeUrl(location)
    // Do a HTTP GET request.
    curl_easy_perform(curlHandle)
    return json.decodeFromString(WeatherResult.serializer(), httpBody)
}

private fun changeUrl(location: String) {
    try {
        curl_easy_setopt(curlHandle, CURLOPT_URL, createUrl(location))
    } catch (ex: IllegalStateException) {
        handleFatalError(ex)
    }
}

private fun handleFatalError(ex: Exception) {
    fprintf(stderr, "${ex.message}\n")
    exitProcess(-1)
}
