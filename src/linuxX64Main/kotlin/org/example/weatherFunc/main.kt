package org.example.weatherFunc

import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import org.example.weatherFunc.model.WeatherResult
import platform.posix.*
import kotlin.system.exitProcess

fun main(): Unit = runBlocking {
	val input = readLine()
	val location = fetchLocationArg(input)
	val jsonFile = fetchJsonFileArg(input)

	@Suppress("CascadeIf")
	if (location.isNotEmpty()) {
		printFromWeatherService(location)
	}
	else if (jsonFile.isNotEmpty()) {
		checkFile(jsonFile)
		printJsonFile(jsonFile)
	}
	else {
		printFunctionUsage()
		exitProcess(0)
	}
	println("Exiting...")
	Unit
}

private fun checkFile(file: String) {
	val error = -1
	if (access(file, F_OK) == error) {
		println("File $file doesn't exist!")
		exitProcess(-1)
	}
}

private suspend fun printFromWeatherService(location: String) {
	println("Fetching weather information (for $location)...")
	println("Weather information:\n${fetchWeather(location)}")
}

private fun printJsonFile(jsonFile: String) {
	// Ignore all JSON fields that aren't covered by the models.
	val json = Json { ignoreUnknownKeys = true }
	println("Printing from JSON file ($jsonFile)...")
	val weather = json.decodeFromString(WeatherResult.serializer(), fetchJson(jsonFile))
	println("Weather Object:\n$weather")
	println("Weather JSON:\n${json.encodeToString(WeatherResult.serializer(), weather)}")
}
