@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.weatherFunc

import kotlinx.cinterop.ByteVar
import kotlinx.cinterop.allocArray
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.toKString
import platform.posix.*

internal actual val apiKey by lazy { fetchApiKey() }

internal fun fetchApiKey(): String {
    val fileName = "openweathermap_key.txt"
	var result = ""
	val bufferLength = 50 * 8
	// Open the file using the fopen function and store the file handle.
	val file = fopen(fileName, "r") ?: throw IllegalStateException("Cannot open $fileName...")

    memScoped {
        val buffer = allocArray<ByteVar>(bufferLength)
        // Read a line from the file using the fgets function.
        result = fgets(__s = buffer, __n = bufferLength, __stream = file)
            ?.toKString()
            ?.replace("\n", "") ?: ""
    }
	// Close the file.
    fclose(file)
	return result
}

internal fun fetchJson(jsonFile: String): String {
	val readMode = "r"
	var result = ""
	// Open the file using the fopen function and store the file handle.
	val file = fopen(jsonFile, readMode)
    fseek(__stream = file, __off = 0, __whence = SEEK_END)
	val fileSize = ftell(file)
    fseek(__stream = file, __off = 0, __whence = SEEK_SET)

    memScoped {
        val buffer = allocArray<ByteVar>(fileSize)
        // Read the entire file and store the contents into the buffer.
        fread(__ptr = buffer, __size = fileSize.toULong(), __n = 1uL, __stream = file)
        result = buffer.toKString()
    }
	// Close the file.
    fclose(file)
	return result
}