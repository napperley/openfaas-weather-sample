package org.example.weatherFunc.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

/** Models the current weather for a location. */
@Serializable
internal data class WeatherResult(
	@SerialName("weather")
	val conditions: List<WeatherCondition>,
	@SerialName("main")
	val climate: Climate,
	val wind: Wind,
	@SerialName("name")
	val placeName: String,
	val sys: System
)
