package org.example.weatherFunc.model

import kotlinx.serialization.Serializable

@Serializable
internal data class Wind(
    val speed: Double,
    val deg: Int
)