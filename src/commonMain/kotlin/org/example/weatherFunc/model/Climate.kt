package org.example.weatherFunc.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class Climate(
    val temp: Double,
    @SerialName("temp_min")
    val minTemp: Double,
    @SerialName("temp_max")
    val maxTemp: Double,
    @SerialName("pressure")
    val windPressure: Int,
    val humidity: Int
)