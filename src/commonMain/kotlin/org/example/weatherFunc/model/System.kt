package org.example.weatherFunc.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class System(
    @SerialName("country")
    val countryCode: String
)
