package org.example.weatherFunc.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class WeatherCondition(
    @SerialName("main")
    val condition: String,
    @SerialName("description")
    val desc: String
)