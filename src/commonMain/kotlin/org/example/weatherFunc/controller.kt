package org.example.weatherFunc

internal expect val apiKey: String

internal fun printFunctionUsage() {
    println(
        """
			| Weather - A OpenFaaS Serverless Function that outputs weather information.
			| Usage examples (pass one of the following as input to the function):
			|   * Location (uses the Open Weather Map service), eg:
			|       -l="christchurch,nz"
			|   * JSON File, eg: -f="weather.json"
			""".trimMargin()
    )
}

internal fun fetchLocationArg(input: String?): String {
	val flag = "-l"
	return if (input != null && input.startsWith("$flag=")) {
		input.replace("$flag=", "").replace("\"", "")
	} else {
		""
	}
}

internal fun fetchJsonFileArg(input: String?): String {
	val flag = "-f"
	return if (input != null && input.startsWith("$flag=")) {
		input.replace("$flag=", "").replace("\"", "")
	} else {
		""
	}
}