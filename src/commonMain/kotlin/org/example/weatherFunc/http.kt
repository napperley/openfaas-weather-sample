package org.example.weatherFunc

import org.example.weatherFunc.model.WeatherResult

internal fun createUrl(location: String): String {
    val baseUrl = "http://api.openweathermap.org/data/2.5/weather"
    return "$baseUrl?q=$location&units=metric&appid=$apiKey"
}

internal expect suspend fun fetchWeather(location: String): WeatherResult
