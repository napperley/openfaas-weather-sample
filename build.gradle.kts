group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.4.10"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.4.10"
}

repositories {
    jcenter()
    mavenCentral()
}

kotlin {
    val jsonVer = "1.0.0"

    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("curl") {
                includeDirs("/usr/include/x86_64-linux-gnu/curl")
            }
            dependencies {
                val coroutinesVer = "1.4.0"
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$jsonVer")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVer")
            }
        }
        binaries {
            executable("weather") {
                entryPoint = "org.example.weatherFunc.main"
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.4.10"
                implementation(kotlin("stdlib-common", kotlinVer))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$jsonVer")
            }
        }
    }
}
